<?php
	header('Content-Type: text/html; charset=UTF-8');
	$user = 'u20398';
	$pass = '7592324';
	$db = new PDO('mysql:host=localhost;dbname=u20398', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

	try {
		$stmt = $db->prepare("SELECT * FROM admin ");
		$stmt -> execute();
	}
	catch(PDOException $e){
		print('Error : ' . $e->getMessage());
		exit();
	}

	$a_data = $stmt->fetchAll(PDO::FETCH_ASSOC)[0];
	$a_login = $a_data['login'];
	$a_hpass = $a_data['hpass'];
		
	// HTTP-аутентификация.
	// PHP хранит логин и пароль в суперглобальном массиве $_SERVER.
	if (empty($_SERVER['PHP_AUTH_USER']) ||
			empty($_SERVER['PHP_AUTH_PW']) ||
			$_SERVER['PHP_AUTH_USER'] != $a_login ||
			md5($_SERVER['PHP_AUTH_PW']) != $a_hpass) {
		header('HTTP/1.1 401 Unauthorized');
		header('WWW-Authenticate: Basic realm="My site"');
		print('<h1>401 Требуется авторизация</h1>');
		exit();
	}

	//print('Вы успешно авторизовались и видите защищенные паролем данные.');

	// *********
	// Здесь нужно прочитать отправленные ранее пользователями данные и вывести в таблицу.
	// Реализовать просмотр и удаление всех данных.
	// *********

	if ($_SERVER['REQUEST_METHOD'] == 'GET') {
		try{ 
			$stmt = $db->prepare("SELECT id, fio, email, login, hpass, birth_date, gender, limbs, superpowers, biographia FROM users");
			$stmt->execute();
		}
		catch(PDOException $e){
			print('Error: ' . $e->getMessage());
			exit();
		}

		$u_data = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$u_values = [];
		foreach ($u_data as $us) {
			$id = !empty($us['id']) ? $us['id'] : '';
			$fio = !empty($us['fio']) ? $us['fio'] : '';
			$email = !empty($us['email']) ? $us['email'] : '';
			$login = !empty($us['login']) ? $us['login'] : '';
			$hpass = !empty($us['hpass']) ? $us['hpass'] : '';
			$birth_date = !empty($us['birth_date']) ? $us['birth_date'] : '';
			$gender = !empty($us['gender']) ? $us['gender'] : '';
			$limbs = !empty($us['limbs']) ? $us['limbs'] : '';
			$superpowers = !empty($us['superpowers']) ? $us['superpowers'] : '';
			$biographia = !empty($us['biographia']) ? $us['biographia'] : '';

			$u_values[$us['id']] = [$id, $fio, $email, $login, $hpass, $birth_date, $gender, $limbs, $superpowers, $biographia];
		}		
		?> 

		<!DOCTYPE html>
		<html lang="ru">
			<head>
					<style>
					.error{
    					border: 2px solid red;
    					background-color: red;
					}
					table {
                      border-collapse: collapse;
                      border: 0.05em solid grey;
                      text-align: center;
                      font-size: 1rem;
                    }
					th {
                      /* заголовоки */
                      border: 0.05em solid grey;
                    }
                    
                    td {
                      /* ячейки */
                      border: 0.05em dashed grey;
                      height: 3em;
                    }
					</style>
					<meta charset="utf-8">
					<title>Admin page</title>
			</head>
			<body>
						<h2>Admin page</h2>
						<div style="text-align: right;"><input type="button" onclick="location.href='index.php'" value="Выход" /></div> 
						<?php
							if(!empty($_COOKIE['id_error'])){
								echo '<div class = "error">Пользователь не найден</div>';
								setcookie('id_error'. '', 1000);
							}
						?>
						<form action="" method="POST">
							<table>
								<tr>
									<th>id</th>
									<th>fio</th>
									<th>email</th>
									<th>login</th>
									<th>hpass</th>
									<th>birth_date</th>
									<th>gender</th>
									<th>limbs</th>
									<th>superpowers</th>
									<th>biographia</th>
								</tr>
								<?php 
									foreach ($u_values as $us){
										echo("<tr>");
										for($i = 0; $i < 10; ++$i){
											echo("<td>");
											print($us[$i]);
											echo("</td>");
										}
										echo("</tr>");
									}
								?>
							</table>
							<br>
							<br>
								<label> Удалить пользователя по id: <br>
								<input name="del_id" />
								</label>								
								<br>
								<br>
								<input type="submit" value="Удалить" />
								<br>
						</form>
			</body>
		</html>

	<?php
	}
	//POST
	else{
		$del_id = [$_POST['del_id']];
		$user = 'u20398';
		$pass = '7592324';
		$db = new PDO('mysql:host=localhost;dbname=u20398', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

		try {
			$stmt = $db->prepare("SELECT * FROM users WHERE id = ?;");
			$stmt -> execute($del_id);
		}
		catch(PDOException $e){
			print('Error : ' . $e->getMessage());
			exit();
		}
		$del_user = $stmt->fetchAll(PDO::FETCH_ASSOC);

		if(!empty($del_user)){
			$stmt = $db->prepare("DELETE FROM users WHERE id = ?;");
			$stmt -> execute($del_id);
			setcookie('save', '1');
			header('Location: admin.php');
		}
		else{
			setcookie('id_error', 1, 0);
			header('Location: admin.php');
		}
	}
?>
