<!DOCTYPE html>

<html lang="ru">

	<head>
		<title>The page</title>
		<meta charset="utf-8">
		<style>
			body{
				text-align: center;
			}
			.error {
				border: 2px solid red;
			}
		</style>
	</head>

	<body>

<?php
if (!empty($messages)) {
	print('<div id="messages">');
	// Выводим все сообщения.
	foreach ($messages as $message) {
		print($message);
	}
	print('</div>');
}
// Далее выводим форму отмечая элементы с ошибками классом error
// и задавая начальные значения элементов ранее сохраненными.
?>
	
		<div style="text-align: right;"><a href="admin.php">Войти как администратор</a></div>
		<?php 
			if(empty($_SESSION['login'])) {
			print(sprintf('<div style="text-align: right;"><a href="login.php">Войти как пользователь</a></div>')); }
		?>
		<h1 id="form">Форма</h1>
		<form action="" method="POST">

			<label>
				ФИО <br />
				<input name="fio" <?php if ($errors['fio']) {print 'class="error"';} ?> value="<?php print $values['fio']; ?>"/>
				<?php if(!empty($messages['fio'])) print($messages['fio']); ?>
			</label><br /><br />

			<label>
				E-mail <br />
				<input name="email" <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>" type="email" placeholder="name@example.com" />
				<?php if(!empty($messages['email'])) print($messages['email']); ?>
			</label><br /><br />

			<label>
				Дата рождения <br />
				<input name="birth_date" <?php if ($errors['birth_date']) {print 'class="error"';} ?> value="<?php print $values['birth_date']; ?>" value="<?php print $values['birth_date']; ?>" type="date" />
				<?php if(!empty($messages['birth_date'])) print($messages['birth_date']); ?>
			</label><br /><br />


			Пол<br />
			<label><input type="radio" name="gender" value="man" <?php if ($errors['gender']) {print 'class="error"';} ?> checked="<?php if($values['gender'] == 'man') {print 'checked';} ?>"/>Муж</label>
			<label><input type="radio" name="gender" value="woman" <?php if ($errors['gender']) {print 'class="error"';} ?> checked="<?php if($values['gender'] == 'woman') {print 'checked';} ?>"/>Жен</label>
			<?php if(!empty($messages['gender'])) print($messages['gender']); ?>
			<br /><br />

			Количество конечностей<br />
			<label><input type="radio" name="limbs" value="2" checked="<?php if($values['limbs'] == '2') {print 'checked';} ?>"/>2</label>
			<label><input type="radio" name="limbs" value="4" checked="<?php if($values['limbs'] == '4') {print 'checked';} ?>"/>4</label>
			<label><input type="radio" name="limbs" value="6" checked="<?php if($values['limbs'] == '6') {print 'checked';} ?>"/>6</label>
			<label><input type="radio" name="limbs" value="8" checked="<?php if($values['limbs'] == '8') {print 'checked';} ?>"/>8</label>
			<?php if(!empty($messages['limbs'])) print($messages['limbs']); ?>
			<br /><br />

			<label>
				Сверхспособности<br />
				<select name="superpowers" multiple="multiple" <?php if ($errors['superpowers']) {print 'class="error"';} ?>>
					<option value="immortality" selected="<?php if (strpos($values['superpowers'], 'immortality')) {print 'selected';} ?>">Бессмертие</option>
					<option value="pass_through_walls" selected="<?php if (strpos($values['superpowers'], 'pass_through_walls')) {print 'selected';} ?>">Прохождение сквозь стены</option>
					<option value="levitation" selected="<?php if (strpos($values['superpowers'], 'levitation')) {print 'selected';} ?>">Левитация</option>
				</select>
				<?php if(!empty($messages['superpowers'])) print($messages['superpowers']); ?>
			</label><br /><br />

			<label>
				Биография<br />
			 <textarea name="biographia" <?php if ($errors['biographia']) {print 'class="error"';} ?> placeholder="Расскажите о себе"><?php print $values['biographia']; ?></textarea>
			 <?php if(!empty($messages['biographia'])) print($messages['biographia']); ?>
			</label><br /><br />

			<label>
				<input type="checkbox" name="checkbox" <?php if ($errors['checkbox']) {print 'class="error"';} ?> checked="<?php if($values['checkbox']) {print 'checked';} ?>"/>С контрактом ознакомлен(а)</label>
				<?php if(!empty($messages['checkbox'])) print($messages['checkbox']); ?>
			<br /><br />

			<input name="send" type="submit" value="Отправить" />
			<br /><br /><br />

		</form>

	</body>

</html>
