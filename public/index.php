<?php
header('Content-Type: text/html; charset=UTF-8');

function randp() { 
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
    $randStr = ''; 
  
    for ($i = 0; $i < 8; $i++) { 
        $index = rand(0, strlen($characters) - 1); 
        $randStr .= $characters[$index]; 
    } 
  
    return $randStr; 
} 

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $messages = array();

    // Выдаем сообщение об успешном сохранении.
    if (!empty($_COOKIE['save'])) {
        setcookie('save', '', 100000);
        setcookie('login', '', 100000);
        setcookie('hpass', '', 100000);
        $messages[] = 'Спасибо, результаты сохранены.';
        // Если в куках есть пароль, то выводим сообщение.
        if (!empty($_COOKIE['hpass'])) {
          $messages[] = sprintf('Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
            и паролем <strong>%s</strong> для изменения данных.',
            strip_tags($_COOKIE['login']),
            strip_tags($_COOKIE['hpass']));
        }
    }

    // Складываем признак ошибок в массив.
    $errors = array();
    $errors['fio'] = !empty($_COOKIE['fio_error']);
    $errors['email'] = !empty($_COOKIE['email_error']);
    $errors['birth_date'] = !empty($_COOKIE['birth_date_error']);
    $errors['gender'] = !empty($_COOKIE['gender_error']);
    $errors['limbs'] = !empty($_COOKIE['limbs_error']);
    $errors['superpowers'] = !empty($_COOKIE['superpowers_error']);
    $errors['biographia'] = !empty($_COOKIE['biographia_error']);
    $errors['checkbox'] = !empty($_COOKIE['checkbox_error']);

    // Выдаем сообщения об ошибках.
    if ($errors['fio']) {
      setcookie('fio_error', '', 100000);
      if($errors['fio'] == 'empty') $messages['fio'] = '<div class="error">Заполните имя.</div>';
      else if ($errors['fio'] == 'invalid') $messages['fio'] = '<div class="error">Имя может содержать только буквы и пробелы.</div>';
    }
    if ($errors['email']) {
      setcookie('email_error', '', 100000);
      if($errors['email'] == 'empty') $messages['email'] = '<div class="error">Заполните email.</div>';
      else if($errors['email'] == 'invalid') $messages['email'] = '<div class="error">Email введён некорректно.</div>';
    }
    if ($errors['birth_date']) {
      setcookie('birth_date_error', '', 100000);
      $messages['birth_date'] = '<div class="error">Заполните дату рождения.</div>';
    }
    if ($errors['gender']) {
      setcookie('gender_error', '', 100000);
      $messages['gender'] = '<div class="error">Укажите пол.</div>';
    }
    if ($errors['limbs']) {
      setcookie('limbs_error', '', 100000);
      $messages['limbs'] = '<div class="error">Укажите количество конечностей.</div>';
    }
    if ($errors['superpowers']) {
      setcookie('superpowers_error', '', 100000);
      $messages['superpowers'] = '<div class="error">Укажите суперспособности.</div>';
    }
    if ($errors['biographia']) {
      setcookie('biographia_error', '', 100000);
      $messages['biographia'] = '<div class="error">Заполните биографию.</div>';
    }
    if ($errors['checkbox']) {
      setcookie('checkbox_error', '', 100000);
      $messages['checkbox'] = '<div class="error">Ознакомьтесь с контрактом.</div>';
    }

    // Складываем предыдущие значения полей в массив, если есть.
    $values = array();
    $values['fio'] = empty($_COOKIE['fio_value']) ? '' : strip_tags($_COOKIE['fio_value']);
    $values['email'] = empty($_COOKIE['email_value']) ? '' : strip_tags($_COOKIE['email_value']);
    $values['birth_date'] = empty($_COOKIE['birth_date_value']) ? '' : $_COOKIE['birth_date_value'];
    $values['gender'] = empty($_COOKIE['gender_value']) ? '' : $_COOKIE['gender_value'];
    $values['limbs'] = empty($_COOKIE['limbs_value']) ? '' : $_COOKIE['limbs_value'];
    $values['superpowers'] = empty($_COOKIE['superpowers_value']) ? '' : $_COOKIE['superpowers_value'];
    $values['biographia'] = empty($_COOKIE['biographia_value']) ? '' : strip_tags($_COOKIE['biographia_value']);
    $values['checkbox'] = empty($_COOKIE['checkbox_value']) ? '' : $_COOKIE['checkbox_value'];

    // Если нет предыдущих ошибок ввода, есть кука сессии, начали сессию и
    // ранее в сессию записан факт успешного логина.
    if (!in_array(1, $errors) &&  session_start() && !empty($_COOKIE[session_name()]) && !empty($_SESSION['login'])) {
      $hpasm = $_SESSION['hpass'];
      $login = $_SESSION['login'];
      $user = 'u20398';
      $pass = '7592324';
      $db = new PDO('mysql:host=localhost;dbname=u20398', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
      // TODO: загрузить данные пользователя из БД
      // и заполнить переменную $values,
      // предварительно санитизовав.
      try{ 
        $stmt = $db->prepare("SELECT fio, email, birth_date, gender, limbs, superpowers, biographia FROM users WHERE login = :login AND hpass = :hpass");
        $stmt->execute(array('login'=>$login, 'hpass'=>$hpasm));
      }
      catch(PDOException $e){
        print('Error : ' . $e->getMessage());
        exit();
      }

      $appData = $stmt->fetchAll(PDO::FETCH_ASSOC)[0];
      $values['fio'] = !empty($appData['fio']) ? $appData['fio'] : '';
      $values['email'] = !empty($appData['email']) ? $appData['email'] : '';
      $values['birth_date'] = !empty($appData['birth_date']) ? $appData['birth_date'] : '';
      $values['gender'] = !empty($appData['gender']) ? $appData['gender'] : '';
      $values['limbs'] = !empty($appData['limbs']) ? $appData['limbs'] : '';
      $values['superpowers'] = !empty($appData['superpowers']) ? $appData['superpowers'] : '';
      $values['biographia'] = !empty($appData['biographia']) ? $appData['biographia'] : '';
      printf('Вход с логином %s, uid %d', $_SESSION['login'], $_SESSION['id']);
    }

    // Включаем содержимое файла form.php.
    // В нем будут доступны переменные $messages, $errors и $values для вывода сообщений, полей с ранее заполненными данными и признаками ошибок.
      include('form.php');
    exit();
}

//если POST
else {
  $errors = FALSE;
  if (empty($_POST['fio'])) {
    setcookie('fio_error', 'empty', time() + 60 * 60);//на час
    $errors = TRUE;
  }
  else if (!preg_match("/^[a-zA-Zа-яёА-ЯЁ]{3,70}/", $_POST['fio'])) {
    setcookie('fio_error', 'invalid', time() + 60 * 60);//на час
    $errors = TRUE;
  }
  else {
    setcookie('fio_value', $_POST['fio'], time() + 365 * 24 * 60 * 60); //на месяц
  }

  if (empty($_POST['email'])) {
    setcookie('email_error', 'empty', time() + 60 * 60);
    $errors = TRUE;
  }
  else if (!preg_match("/^\w+([\.\w]+)*\w@\w((\.\w)*\w+)*\.\w{2,3}$/", $_POST['email'])) {
    setcookie('email_error', 'invalid', time() + 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('email_value', $_POST['email'], time() + 365 * 24 * 60 * 60);
  }

  if (empty($_POST['birth_date'])) {
    setcookie('birth_date_error', '1', time() + 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('birth_date_value', $_POST['birth_date'], time() + 365 * 24 * 60 * 60);
  }

  if (empty($_POST['gender'])) {
    setcookie('gender_error', '1', time() + 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('gender_value', $_POST['gender'], time() + 365 * 24 * 60 * 60);
  }

  if (empty($_POST['limbs'])) {
    setcookie('limbs_error', '1', time() + 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('limbs_value', $_POST['limbs'], time() + 365 * 24 * 60 * 60);
  }

  if (empty($_POST['superpowers'])) {
    setcookie('superpowers_error', '1', time() + 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('superpowers_value', $_POST['superpowers'], time() + 365 * 24 * 60 * 60);
  }

  if (empty($_POST['biographia'])) {
    setcookie('biographia_error', '1', time() + 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('biographia_value', $_POST['biographia'], time() + 365 * 24 * 60 * 60);
  }

  if (empty($_POST['checkbox'])) {
    setcookie('checkbox_error', '1', time() + 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('checkbox_value', $_POST['checkbox'], time() + 365 * 24 * 60 * 60);
  }
  // Сохранить в Cookie признаки ошибок и значения полей.

  if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: index.php');
    exit();
  }
  else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('fio_error', '', 100000);
    setcookie('email_error', '', 100000);
    setcookie('birth_date_error', '', 100000);
    setcookie('gender_error', '', 100000);
    setcookie('limbs_error', '', 100000);
    setcookie('superpowers_error', '', 100000);
    setcookie('biographia_error', '', 100000);
    setcookie('checkbox_error', '', 100000);
  }

  if (!empty($_COOKIE[session_name()]) && session_start() && !empty($_SESSION['login'])) {
    // TODO: перезаписать данные в БД новыми данными,
    // кроме логина и пароля.
    $user = 'u20398';
    $pass = '7592324';
    $db = new PDO('mysql:host=localhost;dbname=u20398', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    
    print('yes');
    
    $fio = $_POST['fio'];
    $email = $_POST['email'];
    $birth_date = $_POST['birth_date'];
    $gender = $_POST['gender'];
    $limbs = $_POST['limbs'];
    $superpowers = $_POST['superpowers'];
    $biographia = $_POST['biographia'];
    $sid = $_SESSION['id'];

    try{
      $stmt = $db->prepare("UPDATE users SET fio = :fio, email = :email, birth_date = :birth_date, gender = :gender, limbs = :limbs, superpowers = :superpowers, biographia = :biographia, WHERE id = :id");
      $stmt -> execute(array('fio'=>$fio, 'email'=>$email, 'birth_date'=>$birth_date, 'gender'=>$gender, 'limbs'=>$limbs, 'superpowers'=>$superpowers, 'biographia'=>$biographia, 'id'=>$sid));
    }
    catch(PDOException $e){
      print('Error : ' . $e->getMessage());
      exit();
    }
  }
  else {
    // Генерируем уникальный логин и пароль.
    // TODO: сделать механизм генерации, например функциями rand(), uniquid(), md5(), substr().
    $login = $_POST['email'];
    $hpass = randp();
    $hpasm = md5($hpass);
    // Сохраняем в Cookies.
    setcookie('login', $login);
    setcookie('hpass', $hpass);

    // TODO: Сохранение данных формы, логина и хеш md5() пароля в базу данных.
    $user = 'u20398';
    $pass = '7592324';
    $db = new PDO('mysql:host=localhost;dbname=u20398', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    
    $fio = $_POST['fio'];
    $email = $_POST['email'];
    $birth_date = $_POST['birth_date'];
    $gender = $_POST['gender'];
    $limbs = $_POST['limbs'];
    $superpowers = $_POST['superpowers'];
    $biographia = $_POST['biographia'];

    try {
      $stmt = $db->prepare("INSERT INTO users (fio, email, login, hpass, birth_date, gender, limbs, superpowers, biographia) VALUES (:fio, :email, :login, :hpass, :birth_date, :gender, :limbs, :superpowers, :biographia)");
      $stmt -> execute(array('fio'=>$fio, 'email'=>$email, 'login'=>$login, 'hpass'=>$hpasm, 'birth_date'=>$birth_date, 'gender'=>$gender, 'limbs'=>$limbs, 'superpowers'=>$superpowers, 'biographia'=>$biographia));
    }
    catch(PDOException $e){
      print('Error : ' . $e->getMessage());
      exit();
    }
  }

  // Сохраняем куку с признаком успешного сохранения.
  setcookie('save', '1');

  // Делаем перенаправление.
  header('Location: index.php');
} 

?>
